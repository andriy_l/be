package be;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import be.entities.Currency;

@SpringBootApplication
@RestController
public class App {
  private final static Logger LOGGER = LoggerFactory.getLogger(App.class);
  
  @Value("classpath:exchange.json")
  static Resource exchange;
  
  private static final double COEFFICIENT = 0.05;
  
  private static final String HOST = "bank.gov.ua";
  private static final String URL = "https://" + HOST + "/NBUStatService/v1/statdirectory/exchange?json";

  private static final int PORT = 443;

  
  public static boolean pingService(String host, int port, int timeout) {
    try (Socket socket = new Socket()) {
        socket.connect(new InetSocketAddress(host, port), timeout);
        return true;
    } catch (IOException e) {
      LOGGER.warn("Either timeout or unreachable or failed DNS lookup.");
        return false; 
    }
}

  private static List<Currency> requestProcessedData() {
    List<Currency> exchangeInJson = Collections.EMPTY_LIST;
    ObjectMapper mapper = new ObjectMapper();
    try {
      boolean reachable = pingService("bank.gov.ua", PORT, 800);
      if(!reachable) {
        exchangeInJson = mapper.readValue(exchange.getFile(), new TypeReference<List<Currency>>(){ });
        LOGGER.info("reading local file");
      } else {
        exchangeInJson = mapper.readValue(new URL(URL).openConnection().getInputStream(), new TypeReference<List<Currency>>(){ });
        LOGGER.info("working with NBU");
      }
    } catch (IOException e) {
      LOGGER.error("Cannot obtain list", e);
    }
    return exchangeInJson;
  }
  
  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

  @GetMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
  public static String hello() {
    return "HELLO IM Fake Bank";
  }

  @GetMapping(value = "/readDataForCode/{code}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public static @ResponseBody ResponseEntity<Currency> requestCodeData(@PathVariable int code) {    
    Currency currency = requestProcessedData()
        .stream()
        .filter(c -> c.getR030() == code)
        .findAny()
        .get();
    return new ResponseEntity<Currency>(currency, HttpStatus.OK);
  }

  @GetMapping(value = "/readDataForSale/{cc}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public static @ResponseBody ResponseEntity<Currency> requestCcDataForSale(@PathVariable String cc) {    
    Currency currency = requestProcessedData()
        .stream()
        .filter(c -> c.getCc().equals(cc))
        .findAny()
        .get();
    currency.setRate(currency.getRate() * COEFFICIENT + currency.getRate());
    return new ResponseEntity<Currency>(currency, HttpStatus.OK);
  }
  
  @GetMapping(value = "/readDataForBuy/{cc}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public static @ResponseBody ResponseEntity<Currency> requestCcDataForBuy(@PathVariable String cc) {    
    Currency currency = requestProcessedData()
        .stream()
        .filter(c -> c.getCc().equals(cc))
        .findAny()
        .get();
    currency.setRate(currency.getRate() - currency.getRate() * COEFFICIENT);
    return new ResponseEntity<Currency>(currency, HttpStatus.OK);
  }
  
  @GetMapping(value = "/readDataForBuy/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public static @ResponseBody ResponseEntity<List<Currency>> requestAllCurrenciesForBuy() {    
    List<Currency> currency = 
        requestProcessedData().stream()
        .map(c -> {c.setRate(c.getRate() - c.getRate() * COEFFICIENT); return c;})
        .collect(Collectors.toList());       
        
    return new ResponseEntity<List<Currency>>(currency, HttpStatus.OK);
  }
  
  
  @GetMapping(value = "/readDataForSale/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public static @ResponseBody ResponseEntity<List<Currency>> requestAllCurrenciesForSale() {    
    List<Currency> currency = 
        requestProcessedData().stream()
        .map(c -> {c.setRate(c.getRate() + c.getRate() * COEFFICIENT); return c;})
        .collect(Collectors.toList());       
        
    return new ResponseEntity<List<Currency>>(currency, HttpStatus.OK);
  }

}
